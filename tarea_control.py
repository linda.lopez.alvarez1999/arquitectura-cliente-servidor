import tkinter as tk
import socket
import sys

#192.168.0.1

def sendSignalTCP(IP, port, signal):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (IP, port)
    try:
        print("Esperando por la conexion con " + IP+  " ...")
        connection, client_address = sock.accept()
        connection.sendall(signal)
        print("señal enviada ... ")
    except:
        print("Fallo en la conexion ... ")

def clickEncender():
    input_ip = ip_text_1.get("1.0",tk.END+"-1c") +"."+ip_text_2.get("1.0",tk.END+"-1c")+"."+ip_text_3.get("1.0",tk.END+"-1c")+"."+ip_text_4.get("1.0",tk.END+"-1c")
    input_port = int(port_textBox.get("1.0",tk.END+"-1c"))
    sendSignalTCP(input_ip, input_port, "Encender")
    toplevel = tk.Toplevel()
    send_text = "Enviando señal de encendido al IP: "+input_ip+" por el puerto "+str(input_port)+" ..."
    label1 = tk.Label(toplevel, text=send_text, height=4, width=100)
    label1.pack()


def clickApagar():
    input_ip = ip_text_1.get("1.0",tk.END+"-1c") +"."+ip_text_2.get("1.0",tk.END+"-1c")+"."+ip_text_3.get("1.0",tk.END+"-1c")+"."+ip_text_4.get("1.0",tk.END+"-1c")
    input_port = int(port_textBox.get("1.0",tk.END+"-1c"))
    sendSignalTCP(input_ip, input_port, "Apagar")
    toplevel = tk.Toplevel()
    send_text = "Enviando señal de apagado al IP: "+input_ip+" por el puerto "+str(input_port)+" ..."
    label1 = tk.Label(toplevel, text=send_text, height=4, width=100)
    label1.pack()



window = tk.Tk()

label_a = tk.Label(master=window, text="IP")
label_a.grid(row=0, column=0)

ip_text_1 = tk.Text(window, height = 1, width = 3)
ip_text_1.grid(row=0, column=1)

ip_label_dot_1 = tk.Label(master=window, text=".")
ip_label_dot_1.grid(row=0, column=2)

ip_text_2 = tk.Text(window, height = 1, width = 3)
ip_text_2.grid(row=0, column=3)

ip_label_dot_2 = tk.Label(master=window, text=".")
ip_label_dot_2.grid(row=0, column=4)

ip_text_3 = tk.Text(window, height = 1, width = 3)
ip_text_3.grid(row=0, column=5)

ip_label_dot_3 = tk.Label(master=window, text=".")
ip_label_dot_3.grid(row=0, column=6)

ip_text_4 = tk.Text(window, height = 1, width = 3)
ip_text_4.grid(row=0, column=7)

label_b = tk.Label(master=window, text="Puerto")
label_b.grid(row=1, column=0)

port_textBox = tk.Text(window, height = 1, widt = 5)
port_textBox.grid(row=1, column=1)

button_encender = tk.Button(window, text="Encender", command=clickEncender)
button_encender.grid(row=2, column=0)

button_apagar = tk.Button(window, text="Apagar", command=clickApagar)
button_apagar.grid(row=2, column=1)

window.mainloop()






